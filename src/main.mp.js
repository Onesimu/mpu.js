import './u/u.js'
import './u/web.js'

import Vue from 'vue'
import App from './App.vue'
import router from './route.js'
import store from './store'

Vue.config.productionTip = false
  
export default function createApp() {
  const container = document.createElement('div')
  container.id = 'app'
  document.body.appendChild(container)

  return new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
  })
}
