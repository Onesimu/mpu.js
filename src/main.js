import './u/u.js'
import './u/web.js'

import Vue from 'vue'
import App from './App.vue'
import router from './route.js'
import store from './store'

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
})
