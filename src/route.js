import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

// 1、首先获取当前目录下所有的文件；
// 2、然后筛选出所有的vue文件;
const files = require.context('./pg/', true, /\.vue$/)

const pages = {}

files.keys().forEach((key) => {
  // 3、通过正则将文件名作为属性名来保存文件
  pages[key.replace(/(\.\/|\.vue)/g, '')] = files(key).default
})

const routes = []

// 通过内置对象Object的keys方法来生成vue可用的路由数组
Object.keys(pages).forEach((item) => {
  routes.push({
    path: `/${item}`,
    name: item,
    component: pages[item],
  })
})

routes.push({
  path: '/',
  redirect: '/index'
})

export default new VueRouter({ routes, mode: 'history' })
