module.exports = {
  origin: 'https://test.miniprogram.com',
  entry: '/',
  router: {
    home: [
      '/',
      '/login'
    ]
  },
  redirect: {
    notFound: 'home',
    accessDenied: 'home',
  },
  generate: {
    autoBuildNpm: false,
  },
  app: {
    backgroundTextStyle: 'dark',
    navigationBarTextStyle: 'white',
    navigationBarTitleText: 'kbone',
  },
  appExtraConfig: {
    sitemapLocation: 'sitemap.json',
    useExtendedLib: {
      kbone: true,
    },
  },
  global: {
    share: true,
    windowScroll: false,
    backgroundColor: '#F7F7F7',
  },
  pages: {},
  optimization: {
    domSubTreeLevel: 10,

    elementMultiplexing: true,
    textMultiplexing: true,
    commentMultiplexing: true,
    domExtendMultiplexing: true,

    styleValueReduce: 5000,
    attrValueReduce: 5000,
  },
  projectConfig: {
    projectname: 'kbone-template-vue',
    appid: '',
  },
}
